from absl import app, flags, logging
from absl.flags import FLAGS
import cv2
import os
import pathlib
import numpy as np
import tensorflow as tf
from tfserver import service_models
from modules.models import RetinaFaceModel
from modules.utils import (set_memory_growth, load_yaml,
                           pad_input_image, recover_pad_output)
from modules import models, losses, service_model

flags.DEFINE_string('cfg_path', './configs/retinaface_res50.yaml',
                    'config file path')
flags.DEFINE_string('gpu', '0', 'which gpu to use')
flags.DEFINE_string('save_folder', './widerface_evaluate/widerface_txt/',
                    'folder path to save evaluate results')
flags.DEFINE_boolean('origin_size', True,
                     'whether use origin image size to evaluate')
flags.DEFINE_boolean('save_image', True, 'whether save evaluation images')
flags.DEFINE_float('iou_th', 0.4, 'iou threshold for nms')
flags.DEFINE_float('score_th', 0.02, 'score threshold for nms')
flags.DEFINE_float('vis_th', 0.5, 'threshold for visualization')


def load_info(txt_path):
    """load info from txt"""
    img_paths = []
    words = []

    f = open(txt_path, 'r')
    lines = f.readlines()
    isFirst = True
    labels = []
    for line in lines:
        line = line.rstrip()
        if line.startswith('#'):
            if isFirst is True:
                isFirst = False
            else:
                labels_copy = labels.copy()
                words.append(labels_copy)
                labels.clear()
            path = line[2:]
            path = txt_path.replace('label.txt', 'images/') + path
            img_paths.append(path)
        else:
            line = line.split(' ')
            label = [float(x) for x in line]
            labels.append(label)

    words.append(labels)
    return img_paths, words


def draw_bbox_landm(img, r):
    """draw bboxes and landmarks"""
    # bbox
    x1, y1, x2, y2 = r ['box']
    cv2.rectangle(img, (x1, y1), (x1 + x2, y1 + y2), (0, 255, 0), 2)
    # confidence
    text = "{:.4f}".format(r['confidence'])
    cv2.putText(img, text, (x1, y1),
                cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255))

    # landmark
    cv2.circle(img, r ['keypoints']['left_eye'], 1, (255, 255, 0), 2)
    cv2.circle(img, r ['keypoints']['right_eye'], 1, (0, 255, 255), 2)
    cv2.circle(img, r ['keypoints']['nose'], 1, (255, 0, 0), 2)
    cv2.circle(img, r ['keypoints']['mouth_left'], 1, (0, 100, 255), 2)
    cv2.circle(img, r ['keypoints']['mouth_right'], 1, (255, 0, 100), 2)

def main(_argv):
    # init
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu

    logger = tf.get_logger()
    logger.disabled = True
    logger.setLevel(logging.FATAL)
    set_memory_growth()

    cfg = load_yaml(FLAGS.cfg_path)
    # define network
    model = RetinaFaceModel(cfg, training=False, iou_th=FLAGS.iou_th,
                            score_th=FLAGS.score_th)
    checkpoint_dir = './checkpoints/' + cfg['sub_name']
    if 0:
        sm = service_models.Model ()
        sm.save ('models', model, checkpoint_dir = checkpoint_dir, assets = [service_model.__file__])
        sm.write_asset ('model-config', cfg)
    else:
        model = service_models.load ('models')
    model.summary ()

    # evaluation on testing dataset
    testset_folder = cfg['testing_dataset_path']
    testset_list = os.path.join(testset_folder, 'label.txt')

    img_paths, _ = load_info(testset_list)
    for img_index, img_path in enumerate(img_paths [:10]):
        print(" [{} / {}] det {}".format(img_index + 1, len(img_paths),
                                         img_path))
        img_raw = cv2.imread(img_path, cv2.IMREAD_COLOR)
        img_height_raw, img_width_raw, _ = img_raw.shape
        img = np.float32(img_raw.copy())

        # testing scale
        target_size = 1600
        max_size = 2150
        img_shape = img.shape
        img_size_min = np.min(img_shape[0:2])
        img_size_max = np.max(img_shape[0:2])
        resize = float(target_size) / float(img_size_min)
        # prevent bigger axis from being more than max_size:
        if np.round(resize * img_size_max) > max_size:
            resize = float(max_size) / float(img_size_max)
        if FLAGS.origin_size:
            if os.path.basename(img_path) == '6_Funeral_Funeral_6_618.jpg':
                resize = 0.5 # this image is too big to avoid OOM problem
            else:
                resize = 1
        img = cv2.resize(img, None, None, fx=resize, fy=resize,
                         interpolation=cv2.INTER_LINEAR)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        results = model.predict (img)

        # write results
        img_name = os.path.basename(img_path)
        sub_dir = os.path.basename(os.path.dirname(img_path))
        save_name = os.path.join(
            FLAGS.save_folder, sub_dir, img_name.replace('.jpg', '.txt'))

        pathlib.Path(os.path.join(FLAGS.save_folder, sub_dir)).mkdir(
            parents=True, exist_ok=True)

        with open(save_name, "w") as file:
            file_name = img_name + "\n"
            bboxs_num = str(len(results)) + "\n"
            file.write(file_name)
            file.write(bboxs_num)
            for result in results:
                x, y, w, h = result ['box']
                confidence = str(result ['confidence'])
                line = str(x) + " " + str(y) + " " + str(w) + " " + str(h) \
                    + " " + confidence + " \n"
                file.write(line)

        # save images
        pathlib.Path(os.path.join(
            './results', cfg['sub_name'], sub_dir)).mkdir(
                parents=True, exist_ok=True)

        if FLAGS.save_image:
            for result in results:
                draw_bbox_landm(img_raw, result)

            cv2.imwrite(os.path.join('./results', cfg['sub_name'], sub_dir,
                                     img_name), img_raw)


if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass


